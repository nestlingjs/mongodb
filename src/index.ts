import { MongoClient } from 'mongodb'
import { ConfigService } from '@nestling/config'
import { LogService } from '@nestling/logger'

export const mongoProvider = {
  provide: 'MongoDbToken',
  useFactory: async (
    config: ConfigService,
    logger: LogService
  ) => {
    const {
      mongo: {
        uri
      }
    } = config as any

    try {
      logger.info(`Connecting to: ${uri}`)
      return MongoClient
        .connect(uri)
    } catch (error) {
      logger.error(error)
      throw Error('Failed to connect')
    }
  },
  inject: [
    ConfigService,
    LogService
  ]
}
