# Mongo Token Provider for NestJS 

Provides a named token 'MongoDbToken'

Depends on both '@nestling/logger' and '@nestling/config'

The config service must provide:
``
mongo: {
  uri: ''
}
``
